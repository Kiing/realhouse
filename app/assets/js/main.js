$('.list').slick({
  responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 850,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


$(document).ready(function () {
  var lastScrollTop = 0;
  $(window).scroll(function (event) {
    var currentScroll = $(this).scrollTop();
    if ($(this).scrollTop() > lastScrollTop) {
      $('.relative').addClass('fixed');
    } else {
      $('.relative').removeClass('fixed');
    }
    //  lastScrollTop = currentScroll;
  });


  $('.menu-mobile').click(function () {
    $(".menu-sidebar").slideToggle("slow");
    $('.menu-sidebar').addClass('show');
    $('.overlay').addClass('show');
  });

  $('.overlay').click(function () {
    if ($(".menu-sidebar").hasClass("show")) {
      $('.overlay').removeClass('show');
      $('.menu-sidebar').removeClass('show');
      $(".menu-sidebar").slideToggle("slow");
    } else {
      $(".search-wrap").slideToggle("slow");
      $('.overlay').removeClass('show');
      $('.search-wrap').removeClass('show');
    }
  });

  $('.search-btn').click(function () {
    $(".search-wrap").slideToggle("slow");
    $('.search-wrap').addClass('show');
    $('.overlay').addClass('show');
  });

});